// exponent operator
let number = 4
let get_cube = number ** 3

console.log(`The cube of ${number} is ${get_cube}`)

// array desctructuring
let address = ['49', 'Nicanor Ramirez St.', 'Brgy Don Manuel', 'Quezon City']

const [house_number, street_name, district, city] = address

console.log(`I live at ${house_number} ${street_name}, ${district}, ${city}`)

// object desctructuring
let animal = {
    species : "Cat",
    animal_name : "Cheshire",
    height : 50,
    weight : 28
}

const {species, animal_name, height, weight} = animal

console.log(`${animal_name} was a ${species}. He weighed at ${weight}kg with a height of ${height}cm`)

// implicit return 
let numbers = [1, 2, 3, 4, 5]

numbers.forEach((number) => console.log(number))

// class-based constructor
class Dog{
    constructor(name, age, breed){
        this.name = name
        this.age = age
        this.breed = breed
    }
}

console.log(bruno = new Dog('Bruno', 2, 'Boston Terrier'))

